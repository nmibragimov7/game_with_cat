import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import { apiURL } from '../../../config';

const useStyles = makeStyles({
  root: {
    width: "200px",
    height: "auto",
    margin: "10px 0",
    padding: "10px",
    border: "1px solid",
    borderColor: "rgb(187, 195, 204)",
    textAlign: "center",
  },
});

const CardImage = (props) => {
  const classes = useStyles();
  const [imageClasses, setImageClasses] = useState({});

  let image = "";
  if(props.image) {
    image = apiURL + "/uploads/" + props.image;
  }

  useEffect(() => {
    setImageClasses({});
    if(props.imageIndex === props.index) {
      setImageClasses({border: "1px solid rgb(0, 123, 255)", background: "rgb(248, 249, 250)"});
    }
    
  }, [props.imageIndex])

  return (
    <>
      <Card className={classes.root} onClick={props.selectImage} style={imageClasses}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt={props.name}
            height="150"
            image={image}
          />
        </CardActionArea>
      </Card>
    </>
  );
}

export default CardImage;