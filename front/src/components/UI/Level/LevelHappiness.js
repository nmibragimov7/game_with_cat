import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: "5px"
    },
}));

const LevelHappiness = (props) => {
    const classes = useStyles();
    const [value, setValue] = useState(1);
    
    useEffect(() => {
        let level = 1
        if(props.level >= 10) {
            level = Math.round(props.level/20);
        }
        setValue(level);
    }, [props.level]);

    const customIcons = {
        1: {
            icon: <SentimentVeryDissatisfiedIcon />,
        },
        2: {
            icon: <SentimentDissatisfiedIcon />,
        },
        3: {
            icon: <SentimentSatisfiedIcon />,
        },
        4: {
            icon: <SentimentSatisfiedAltIcon />,
        },
        5: {
            icon: <SentimentVerySatisfiedIcon />,
        },
    };

    const IconContainer = (props) => {
        const { value, ...other } = props;
        return <span {...other}>{customIcons[value].icon}</span>;
    }

    IconContainer.propTypes = {
        value: PropTypes.number.isRequired,
    };

    return (
        <>
            <Box component="fieldset" mt={1} borderColor="transparent">
                <Rating
                className={classes.root}
                name="customized-icons"
                value={value}
                readOnly
                IconContainerComponent={IconContainer}
                />
            </Box>
        </>
    );
}

export default LevelHappiness;