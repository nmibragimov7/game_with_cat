import React, { useEffect, useState } from 'react';
import Rating from '@material-ui/lab/Rating';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Box from '@material-ui/core/Box';

const LevelSatiety = (props) => {
    const [value, setValue] = useState(0);
    
    useEffect(() => {
        let level = Math.round(props.level/20*2)/2;
        setValue(level);
    }, [props.level]);

    return (
        <>
            <Box component="fieldset" mt={1} borderColor="transparent">
                <Rating
                name="customized-color"
                value={value}
                precision={0.5}
                readOnly
                icon={<FavoriteIcon fontSize="inherit" />}
                />
            </Box>
        </>
    );
}

export default LevelSatiety;