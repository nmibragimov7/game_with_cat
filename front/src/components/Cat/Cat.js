import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { apiURL } from "../../config";
import LevelHappiness from '../UI/Level/LevelHappiness';
import LevelSatiety from '../UI/Level/LevelSatiety';
import ActionsForm from '../ActionsForm/ActionsForm';
import SingleBedIcon from '@material-ui/icons/SingleBed';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';
import LocalDiningIcon from '@material-ui/icons/LocalDining';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        alignItems: "center",
        border: "1px solid",
        borderColor: "rgb(187, 195, 204)",
        padding: "20px",
        '&:hover': {
            color: 'inherit',
            background: "rgb(248, 249, 250)",
            borderColor: "rgb(0, 123, 255)"
        }
    },
    rootGame: {
        display: "flex",
        alignItems: "top",
        border: "1px solid",
        borderColor: "rgb(187, 195, 204)",
        padding: "20px",
    },
    content: {
        width: "100%",
        display: "flex",
        justifyContent: "space-between",
        marginLeft: "20px"
    },
    image: {
        display: "block",
        width: "150px",
        height: "150px",
        marginRight: "40px",
        borderRadius: "50%",
    },
    imageGame: {
        display: "block",
        width: "250px",
        height: "250px",
        marginRight: "20px",
        borderRadius: "50%"
    },
}));

const Cat = (props) => {
    const classes = useStyles();
    const [imageClasses, setImageClasses] = useState({});
    
    let image = "";
    if(props.image) {
        image = apiURL + "/uploads/" + props.image;
    }
    
    const errorHandler = () => {
        setImageClasses({visibility: "hidden"});
    }
    
    return (
        <>
            {props.gameCat ? (
                <div className={classes.rootGame}>
                    <img src={image} alt={`${props.name}`} className={classes.imageGame} style={imageClasses} onError={errorHandler}/> 
                    <div className={classes.content}>
                        <div>
                            <h1>Cat: {props.name}</h1>
                            <p>Age: {props.age} year</p>
                            <span>Level happiness:</span>
                            <LevelHappiness 
                            level={props.levelHappiness}
                            />
                            <span>Level satiety:</span>
                            <LevelSatiety 
                            level={props.levelSatiety}
                            />
                            <ActionsForm
                            onSubmit={props.actionHandler}
                            />
                        </div>
                        <div>
                            {props.state === "sleep" ? (
                            <SingleBedIcon
                            color="primary"
                            fontSize="large"
                            />
                            ) : (
                                <>
                                    {props.state === "play" ? (
                                        <SportsEsportsIcon
                                        color="action"
                                        fontSize="large"
                                        />
                                    ) : (
                                        <>
                                            {props.state === "eat" ? (
                                                <LocalDiningIcon
                                                color="secondary"
                                                fontSize="large"
                                                />
                                            ) : null}
                                        </>
                                    )}
                                </>
                            )}
                        </div>
                    </div>
                </div>
            ) : (
                <div className={classes.root} onClick={props.blockHandler}>
                    <img src={image} alt={`${props.name}`} className={classes.image} style={imageClasses} onError={errorHandler}/> 
                    <h3>{props.name}</h3>
                </div>
            )}
        </>
    );
};

export default Cat;