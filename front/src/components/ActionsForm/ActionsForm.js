import React, { useState } from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import SelectCategory from '../UI/Form/SelectCategory';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        textAlign: "center"
    }
}));

const ActionsForm = ({onSubmit}) => {
    const classes = useStyles();

    const actions = ["feeding", "game", "sleep",];
    const [action, setAction] = useState("");

    const inputChangeHandler = event => {
        const value = event.target.value;
        setAction(value);
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        onSubmit(action);
    };

    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <SelectCategory
                        inputs={action}
                        name="actions"
                        categories={actions}
                        inputChangeHandler={inputChangeHandler}
                        action={true}
                    />
                </Grid> 
                <Grid item>
                    <Button type="submit" color="primary" variant="contained">Confirm</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default ActionsForm;