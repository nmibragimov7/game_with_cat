import React, { useState } from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import TextInput from "../UI/Form/TextInput";
import SelectCategory from '../UI/Form/SelectCategory';
import CardImage from '../UI/CardImage/CardImage';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),
        textAlign: "center"
    }
}));

const NewCatForm = ({images, characters, onSubmit}) => {
    const classes = useStyles();
    const [imageIndex, setImageIndex] = useState(0);

    const [state, setState] = useState({
        name: "",
        image: "",
        character: ""
    });

    const errors = useSelector(state => state.cats.errors);

    const selectImage = (id, index) => {
        setImageIndex(index + 1);
        setState(prevState => {
            return {...prevState, image: id}
        })
    };

    const inputChangeHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        })
    };
    
    const submitFormHandler = event => {
        event.preventDefault();
        onSubmit(state);
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <form
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
        >
            <Grid container direction="column" spacing={2}>
                <Grid item container direction="row" spacing={2}>
                    {images.map((image, index) => (
                        <Grid item key={image._id}>
                            <CardImage
                                selectImage={() => selectImage(image._id, index)}
                                image={image.image}
                                index={index + 1}
                                imageIndex={imageIndex}
                            />
                        </Grid>
                    ))}
                </Grid>
                <TextInput
                    label="name"
                    onChange={inputChangeHandler}
                    name="name"
                    error={getFieldError('name')}
                    required={true}
                />
                <Grid item>
                    <SelectCategory
                        inputs={state.character}
                        name="character"
                        categories={characters}
                        inputChangeHandler={inputChangeHandler}
                    />
                </Grid> 
                <Grid item>
                    <Button type="submit" color="primary" variant="contained">Create new cat</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default NewCatForm;