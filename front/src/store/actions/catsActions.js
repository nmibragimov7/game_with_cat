import axios from "../../axiosApi";
import { loadingOffHandler } from "./loadingActions";
import { push } from "connected-react-router";

export const RESET_ERROR = 'RESET_ERROR';
export const RESET_MESSAGE = 'RESET_MESSAGE';
export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS';
export const FETCH_CHARACTERS_SUCCESS = 'FETCH_CHARACTERS_SUCCESS';
export const FETCH_CATS_SUCCESS = 'FETCH_CATS_SUCCESS';
export const FETCH_CAT_SUCCESS = 'FETCH_CAT_SUCCESS';
export const CREATE_CAT_SUCCESS = 'CREATE_CAT_SUCCESS';
export const CREATE_CAT_FAILURE = 'CREATE_CAT_FAILURE';
export const SEND_ACTION_SUCCESS = 'SEND_ACTION_SUCCESS';
export const SEND_ACTION_FAILURE = 'SEND_ACTION_FAILURE';

export const resetError = () => {
    return {type: RESET_ERROR}
};
export const resetMessage = () => {
    return {type: RESET_MESSAGE}
};
export const fetchImagesSuccess = (images) => {
    return {type: FETCH_IMAGES_SUCCESS, images}
};
export const fetchCharactersSuccess = (characters) => {
    return {type: FETCH_CHARACTERS_SUCCESS, characters}
};
export const fetchCatsSuccess = (cats) => {
    return {type: FETCH_CATS_SUCCESS, cats}
};
export const fetchCatSuccess = (cat) => {
    return {type: FETCH_CAT_SUCCESS, cat}
};
export const createCatSuccess = (message) => {
    return {type: CREATE_CAT_SUCCESS, message}
};
export const createCatFailure = (errors) => {
    return {type: CREATE_CAT_FAILURE, errors}
};
export const sendActionSuccess = (message) => {
    return {type: SEND_ACTION_SUCCESS, message}
};
export const sendActionFailure = (errors) => {
    return {type: SEND_ACTION_FAILURE, errors}
};

export const fetchImages = () => {
    return async dispatch => {
        try {
            const response = await axios.get('/images');
            dispatch(fetchImagesSuccess(response.data));
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchCharacters = () => {
    return async dispatch => {
        try {
            const response = await axios.get('/characters');
            dispatch(fetchCharactersSuccess(response.data));
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchCats = () => {
    return async dispatch => {
        try {
            const response = await axios.get('/cats');
            dispatch(fetchCatsSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const fetchCat = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get(`/cats/${id}`);
            dispatch(fetchCatSuccess(response.data));
            dispatch(loadingOffHandler());
        } catch(e) {
            console.error(e);
        }
    }
};

export const createCat = (catData) => {
    return async (dispatch) => {
        try {
            const response = await axios.post(`/cats`, catData);
            dispatch(createCatSuccess(response.data));
            dispatch(push('/my_cats'));
        } catch(e) {
            dispatch(createCatFailure(e.response.data));
            console.error(e);
        }
    }
};

export const sendAction = (action, id) => {
    return async dispatch => {
        try {
            const response = await axios.post(`/cats/${id}?action=${action}`, null);
            await dispatch(sendActionSuccess(response.data));
            dispatch(fetchCat(id));
        } catch(e) {
            if(e.response) {
                dispatch(sendActionFailure(e.response.data));
            } else {
                dispatch(sendActionFailure({error: 'No internet'}));
            }
            console.error(e);
        }
    }
};