import {
    RESET_ERROR,
    RESET_MESSAGE,
    FETCH_IMAGES_SUCCESS,
    FETCH_CHARACTERS_SUCCESS,
    FETCH_CATS_SUCCESS,
    FETCH_CAT_SUCCESS,
    CREATE_CAT_SUCCESS,
    CREATE_CAT_FAILURE,
    SEND_ACTION_SUCCESS,
    SEND_ACTION_FAILURE
} from "../actions/catsActions";

const initialState = {
    cats: [],
    cat: {},
    errors: null,
    message: null,
    images: [],
    characters: []
};

const catsReducer = (state = initialState, action) => {
    switch(action.type){
        case RESET_ERROR:
            return {...state, errors: null};
        case RESET_MESSAGE:
            return {...state, message: null};
        case FETCH_IMAGES_SUCCESS:
            return {...state, images: action.images}
        case FETCH_CHARACTERS_SUCCESS:
            return {...state, characters: action.characters}
        case FETCH_CATS_SUCCESS:
            return {...state, cats: action.cats}
        case FETCH_CAT_SUCCESS:
            return {...state, cat: action.cat}
        case CREATE_CAT_SUCCESS:
            return {...state, errors: null, message: action.message}
        case CREATE_CAT_FAILURE:
            return {...state, errors: action.errors}
        case SEND_ACTION_SUCCESS:
            return {...state, errors: null, message: action.message}
        case SEND_ACTION_FAILURE:
            return {...state, errors: action.errors}
        default:
            return state;
    }
};

export default catsReducer;