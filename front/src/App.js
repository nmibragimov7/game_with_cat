import React from 'react';
import Container from "@material-ui/core/Container";
import { Route, Switch, Redirect } from "react-router-dom"
import AppToolbar from "./components/AppToolbar/AppToolbar";
import Register from "./containers/User/Register";
import Login from "./containers/User/Login";
import { useSelector } from "react-redux";
import MainPage from './containers/MainPage/MainPage';
import NewCat from './containers/NewCat/NewCat';
import MyCats from "./containers/MyCats/MyCats";
import GameCat from './containers/GameCat/GameCat';

const App = () => {
  const user = useSelector(store => store.users.user);

  return(
    <>
      <header><AppToolbar user={user}/></header>
      <main>
        <Container>
            <Switch>
              <Route path="/" exact component={MainPage}/>
              <ProtectRoute path="/my_cats" isAllowed={user} redirectTo={'/'} exact component={MyCats}/>
              <ProtectRoute path="/new_cat" isAllowed={user} redirectTo={'/'} exact component={NewCat}/>
              <Route path="/register" exact component={Register}/>
              <Route path="/login" exact component={Login}/>
              <Route path="/:id" component={GameCat}/>
              <Route path='/' render={()=>(<div><h1>404 Not found</h1></div>)}/>
            </Switch>
        </Container>
      </main>
    </>
  );
};

const ProtectRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
      <Route {...props} /> :
      <Redirect to={redirectTo} />
};

export default App;
