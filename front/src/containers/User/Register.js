import React, {useState, useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import {Link as RouterLink} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {registerUser, resetError} from "../../store/actions/usersAction";
import {useDispatch, useSelector} from "react-redux";
import TextInput from "../../components/UI/Form/TextInput";
import FileInput from '../../components/UI/Form/FileInput';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const Register = (props) => {
    const classes = useStyles();
    const [state, setState] = useState({
        username: "",
        displayName: "",
        password: "",
        avatarImage: ""
    });

    const dispatch = useDispatch();
    const errors = useSelector(state => state.users.registerError);

    useEffect(()=> {
        dispatch(resetError());
    }, [dispatch]);

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState =>({
            ...prevState,
            [name]: file
            })
        )
    };

    const inputHandler = event => {
        const {name, value} = event.target;
        setState(prevState => {
            return {...prevState, [name]: value}
        });
    };

    const formSubmitHandler = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key])
        });
        await dispatch(registerUser(formData));
    };

    const getFieldError = fieldName => {
        try {
            return errors.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Register User
                </Typography>
                <form className={classes.form} noValidate onSubmit={formSubmitHandler}>
                    <Grid container spacing={2}>
                        <TextInput
                            label="Username"
                            onChange={inputHandler}
                            name="username"
                            error={getFieldError('username')}
                            required={true}
                        />
                        <TextInput
                            label="Display name"
                            onChange={inputHandler}
                            name="displayName"
                            error={getFieldError('displayName')}
                            required={true}
                        />
                        <TextInput
                            label="Password"
                            onChange={inputHandler}
                            name="password"
                            error={getFieldError('password')}
                            required={true}
                            type="password"
                        />
                        <Grid item>
                            <FileInput
                                name="avatarImage"
                                label="Avatar image"
                                onChange={fileChangeHandler}
                                error={getFieldError('avatarImage')}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link to="/login" variant="body2" component={RouterLink}>
                                Already have an account? Sign in
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}

export default Register;