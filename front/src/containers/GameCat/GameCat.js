import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCat, sendAction, resetMessage } from '../../store/actions/catsActions';
import { loadingHandler } from '../../store/actions/loadingActions';
import Spinner from "../../components/Spinner/Spinner";
import Cat from '../../components/Cat/Cat';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const GameCat = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cat = useSelector(state => state.cats.cat);
    const loading = useSelector(state => state.loading.loading);
    const errors = useSelector(state => state.cats.errors);
    const message = useSelector(state => state.cats.message);

    useEffect(() => {
        dispatch(loadingHandler());
        dispatch(fetchCat(props.match.params.id));
    }, [dispatch]);

    let image = "";
    if(cat.image) {
        image = cat.image.image;
    }

    const actionHandler = (action) => {
        dispatch(sendAction(action, props.match.params.id));
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    let form = (
        <>
            <Cat 
            state={cat.state}
            name={cat.name}
            image={image}
            age={cat.age}
            levelHappiness={cat.levelHappiness}
            levelSatiety={cat.levelSatiety}
            gameCat={true}
            actionHandler={actionHandler}
            />
        </>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {errors && errors.error &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.error}
                </Alert>
            }
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default GameCat;