import React, {useEffect} from 'react';
import { useDispatch, useSelector } from "react-redux";
import NewCatForm from '../../components/NewCatForm/NewCatForm';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";
import { fetchImages, fetchCharacters, createCat, resetError } from '../../store/actions/catsActions';

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    }
}));

const NewCat = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const errors = useSelector(state => state.cats.errors);
    const images = useSelector(state => state.cats.images);
    const characters = useSelector(state => state.cats.characters);
    
    useEffect(()=> {
        dispatch(resetError());
        dispatch(fetchImages());
        dispatch(fetchCharacters());
    }, [dispatch]);

    const onSubmit = (data) => {
        dispatch(createCat(data));
    };

    return (
        <>
            {errors && errors.errors &&
                <Alert
                    severity="error"
                    className={classes.alert}
                >
                    {errors.message}
                </Alert>
            }
            <NewCatForm 
            images={images}
            characters={characters}
            onSubmit={onSubmit}
            />
        </>
    );
};

export default NewCat;