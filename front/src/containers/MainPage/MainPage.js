import React from 'react';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        border: "1px solid #000",
        margin: "0 auto",
        width: "300px",
        textAlign: "center",
    },
}));

const MainPage = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <h3>Welcome</h3>
            <p>To start the game, log in or register</p>
        </div>
    );
};

export default MainPage;