import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCats, resetMessage } from '../../store/actions/catsActions';
import Cat from '../../components/Cat/Cat';
import { loadingHandler } from '../../store/actions/loadingActions';
import Spinner from "../../components/Spinner/Spinner";
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    alert: {
        marginTop: theme.spacing(3),
        width: '100%'
    },
}));

const MyCats = (props) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cats = useSelector(state => state.cats.cats);
    const loading = useSelector(state => state.loading.loading);
    const message = useSelector(state => state.cats.message);

    useEffect(()=> {
        dispatch(loadingHandler());
        dispatch(fetchCats());
    }, [dispatch]);

    const blockHandler = (id) => {
        props.history.push({
            pathname: `/${id}`,
        });
    };

    const closeAlert = () => {
        dispatch(resetMessage());
    };

    let form = (
        <>
            {cats.map(cat => (
                <Cat 
                key={cat._id}
                name={cat.name}
                image={cat.image.image}
                blockHandler={() => blockHandler(cat._id)}
                />
            ))}
        </>
    )

    if (loading) {
        form = <Spinner />;
    }

    return (
        <>
            {message && message.message &&
                <Alert 
                onClose={closeAlert}
                severity="success"
                className={classes.alert}
                >
                    {message.message}
                </Alert>
            }
            {form}
        </>
    );
};

export default MyCats;