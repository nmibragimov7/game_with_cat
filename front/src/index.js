import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { store, history } from "./store/configureStore";
import App from './App'
import axiosApi from './axiosApi';

axiosApi.interceptors.request.use(req => {
  try {
    req.headers['Authorization'] = store.getState().users.user ? store.getState().users.user.token : '';
  } catch (e) {
      throw new Error(e);
  }
  return req;
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
