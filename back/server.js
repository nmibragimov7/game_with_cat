const express = require('express');
const images = require('./app/images');
const characters = require('./app/characters');
const cats = require('./app/cats');
const users = require('./app/users');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./app/config');

const app = express();
const port = 8000;

const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200 
};

app.use(cors(corsOptions));
app.use(express.static('public'));
app.use(express.json());

const run = async () => {
    await mongoose.connect(config.db.url + '/' + config.db.name);
    console.log('Mongo connected');

    app.use('/images', images());
    app.use('/characters', characters());
    app.use('/cats', cats());
    app.use('/users', users());
    
    app.listen(port, () => {
        console.log('Server started on port ' + port);
    });
};

run().catch(console.error);
