const mongoose = require('mongoose');
const config = require('./app/config');
const User = require('./app/models/User');
const Cat = require('./app/models/Cat');
const Image = require('./app/models/Image');
const Character = require('./app/models/Character');
const { nanoid } = require('nanoid');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('cats');
        await db.dropCollection('images');
        await db.dropCollection('characters');
    } catch (e) {
        console.log('Collection not found. Drop collections skiped...');
    }

    const [user1, user2] = await User.create({
        username: "admin",
        displayName: "Admin",
        password: "admin",
        avatarImage: "image.png",
        token: nanoid(),
    }, {
        username: "user",
        displayName: "User",
        password: "user",
        avatarImage: "image.png",
        token: nanoid(),
    });

    const [image1, image2, image3] = await Image.create({
        image: "cat1.jpg",
    }, {
        image: "cat2.jpg",
    }, {
        image: "cat3.jpg",
    });

    const [character1, character2, character3] = await Character.create({
        character: "easy",
    }, {
        character: "normal",
    }, {
        character: "complex",
    });

    await Cat.create({
        user: user1._id,
        name: "Cat",
        image: image1._id,
        character: character1._id,
    });

    await db.close();
});