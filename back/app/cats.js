const express = require('express');
const multer = require('multer');
const router = express.Router();
const {nanoid} = require('nanoid');
const path = require('path');
const config = require('./config');
const Cat = require('./models/Cat');
const auth = require('./middleware/auth');
const mongoose = require('mongoose');
const permitFound = require('./middleware/permitFound');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const createRouter = () =>{
    router.get('/', auth, async (req, res) => {
        try {
            const cocktails = await Cat.find({user: req.user._id}).populate("image");
            res.send(cocktails);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/:id', auth, async (req, res) => {
        try {
            const cat = await Cat.findOne({_id: req.params.id}).populate("image character");
            
            res.send(cat);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/', auth, async (req, res) => {
        const cat = new Cat({
            name: req.body.name,
            image: req.body.image,
            character: req.body.character,
        });
        cat.user = req.user._id;

        try{
            await cat.save();
            res.send({message: "Your cat is created successful"});
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/:id', [auth, permitFound], async (req, res) => {  
        let indexUp = 0;
        let indexDown = 0;
        if(req.cat.character.character === "easy") {
            indexUp = 15/10;
            indexDown = 1/5;
        }
        if(req.cat.character.character === "normal") {
            indexUp = 1;
            indexDown = 1;
        }
        if(req.cat.character.character === "complex") {
            indexUp = 1/5;
            indexDown = 1.5;
        }
        if(req.cat.state === "sleep" && req.query.action === "feeding") {
            return res.send({message: `Sleeping cat cannot be fed`});
        }

        if(req.cat.state === "sleep" && req.query.action === "game") {
            try{
                req.cat.state = "play";
                req.cat.levelHappiness = req.cat.levelHappiness - 5 * indexDown;

                if(req.cat.levelHappiness < 0) {
                    req.cat.levelHappiness = 0;
                }

                await req.cat.save();
                return res.send({message: `Decreased level of happiness by ${5 * indexDown} units`});
            } catch (e) {
                res.status(500).send(e);
            }
        }

        if(req.cat.state !== "sleep" && req.query.action === "feeding") {
            try{
                req.cat.state = "eat";
                if(req.cat.levelSatiety + 15 > 100) {
                    req.cat.levelSatiety = 100;
                    req.cat.levelHappiness = req.cat.levelHappiness - 30 * indexDown;

                    if(req.cat.levelHappiness < 0) {
                        req.cat.levelHappiness = 0;
                    }

                    await req.cat.save();
                return res.send({message: `Decreased level of happiness by ${30 * indexDown}`});
                } else {
                    req.cat.levelSatiety = req.cat.levelSatiety + 15;
                    req.cat.levelHappiness = req.cat.levelHappiness + 5 * indexUp;

                    if(req.cat.levelHappiness > 100) {
                        req.cat.levelHappiness = 100;
                    }
                    if(req.cat.levelSatiety > 100) {
                        req.cat.levelSatiety = 100;
                    }

                    await req.cat.save();
                    return res.send({message: `Increased level of satiety by 15 and happiness by ${5 * indexUp} units`});
                }
            } catch (e) {
                res.status(500).send(e);
            }
        }
        
        if(req.cat.state !== "sleep" && req.query.action === "game") {
            try{
                req.cat.state = "play";

                const random = Math.floor(Math.random() * Math.floor(3));
                if(random < 1) {
                    req.cat.levelHappiness = 0;

                    await req.cat.save();
                    return res.send({message: `Decreased level of satiety by 10 units, level of happiness dropped to 0`});
                } else {
                    req.cat.levelHappiness = req.cat.levelHappiness + 15 * indexUp;
                    req.cat.levelSatiety = req.cat.levelSatiety - 10;

                    if(req.cat.levelHappiness > 100) {
                        req.cat.levelHappiness = 100;
                    }
                    if(req.cat.levelSatiety < 0) {
                        req.cat.levelSatiety = 0;
                    }

                    await req.cat.save();
                    return res.send({message: `Decreased level of satiety by 10 units, increased level of happiness by ${15 * indexUp} units`});
                }
            } catch (e) {
                res.status(500).send(e);
            }
        }

        if(req.query.action === "sleep") {
            try{
                if(req.cat.state !== "sleep") {
                    req.cat.state = "sleep";
                    req.cat.levelHappiness = req.cat.levelHappiness + 20 * indexUp;

                    if(req.cat.levelHappiness > 100) {
                        req.cat.levelHappiness = 100;
                    }

                    await req.cat.save();
                    return res.send({message: `Cat fell asleep. Increased level of happiness by ${20 * indexUp} units`});
                }
                return res.send({message: `Cat has already fallen asleep`});
            } catch (e) {
                res.status(500).send(e);
            }
        }
    });

    return router;
};

module.exports = createRouter;