const Cat = require('../models/Cat');

const permitFound = async (req, res, next) => {
    let cat = null
    try {
        cat = await Cat.findOne({_id: req.params.id}).populate("image character");

    } catch (error) {
        return res.status(403).send({error: "Cat is not found"});
    }

    req.cat = cat;
    next();
};

module.exports = permitFound;