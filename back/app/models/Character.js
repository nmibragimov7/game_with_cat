const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CharacterSchema = new Schema({
    character: {
        type: String,
        required: true,
        enum : ["easy", "normal", "complex"],
        default: "normal"
    }
    }, {versionKey: false});

const Character = mongoose.model('Character', CharacterSchema);

module.exports = Character;