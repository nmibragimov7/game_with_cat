const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CatSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    image: {
        type: Schema.Types.ObjectId,
        ref: 'Image',
        required: true
    },
    levelSatiety: {
        type: Number,
        required: true,
        default: 100,
        min: 0,
        max: 100
    },
    levelHappiness : {
        type: Number,
        required: true,
        default: 100,
        min: 0,
        max: 100
    },
    character: {
        type: Schema.Types.ObjectId,
        ref: 'Character',
        required: true
    },
    state: {
        type: String,
        required: true,
        enum : ["sleep", "play", "eat"],
        default: "sleep"
    },
    age: {
        type: Number,
        default: 1
    }
    }, {versionKey: false});

const Cat = mongoose.model('Cat', CatSchema);

module.exports = Cat;