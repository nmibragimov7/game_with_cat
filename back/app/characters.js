const express = require('express');
const router = express.Router();
const Character = require('./models/Character');

const createRouter = () =>{
    router.get('/', async (req, res) => {
        try {
            const characters = await Character.find();
            res.send(characters);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    return router;
};

module.exports = createRouter;