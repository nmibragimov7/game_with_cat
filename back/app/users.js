const express = require("express");
const User = require("./models/User");
const router = express.Router();
const auth = require('./middleware/auth');
const {nanoid} = require('nanoid');
const config = require('./config');
const multer = require('multer');
const path = require('path');
const axios = require('axios');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({storage});

const createRouter = () => {
    router.post("/", upload.single('avatarImage'), async (req, res) => {
        try {
            const user = new User({
                username: req.body.username,
                displayName: req.body.displayName,
                password: req.body.password,
            });

            if(req.file){
                user.avatarImage = req.file.filename;
            }

            user.generateToken();
            await user.save();
            res.send(user);
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.post("/sessions", async (req, res) => {
        const user = await User.findOne({username: req.body.username});
        if (!user) {
            return res.status(400).send({error: "Username not found"});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({error: "Password is wrong"});
        }

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send(user);
    });

    router.delete('/sessions', auth, async (req, res) => {
        const user = req.user;
        const message = {message: 'Success'};

        user.token = nanoid();
        await user.save({validateBeforeSave: false});
        res.send(message);
    });

    router.post('/facebookLogin', async (req, res) => {
        const inputToken = req.body.accessToken;
        const accessToken = `${config.facebook.appID}|${config.facebook.appSecret}`;

        const tokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

        try {
            const response = await axios.get(tokenUrl);

            if(response.data.error){
                return res.status(401).send({messages: 'Facebook token error'});
            }
            if(req.body.userID !== response.data.data.user_id){
                return res.status(401).send({message: 'Facebook Wrong user id'});
            }

            let user = await User.findOne({username: req.body.userID});

            if(!user){
                user = new User({
                    username: req.body.userID,
                    displayName: req.body.name,
                    password: nanoid(),
                    avatarImage: `${req.body.picture.data.url}`
                })
            }
            user.generateToken();
            await user.save({validateBeforeSave: false});
            return res.send({message: 'Facebook Login or register successful', user});
        } catch (error) {
            return res.status(401).send({messages: 'Facebook token error', error});
        }
    });
    return router;
};

module.exports = createRouter;