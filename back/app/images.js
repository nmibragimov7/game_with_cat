const express = require('express');
const router = express.Router();
const Image = require('./models/Image');

const createRouter = () =>{
    router.get('/', async (req, res) => {
        try {
            const images = await Image.find();
            res.send(images);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    return router;
};

module.exports = createRouter;